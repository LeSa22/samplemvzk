package test.maven.baitap.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "coquan", catalog = "baitap")
public class CoQuan {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COQUAN_ID")
	private int coQuanId;

	@Column(name = "TENCOQUAN", unique = true, length = 255)
	private String tenCoQuan;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "coQuan", cascade = CascadeType.ALL)
	private List<VanBan> listVanBan;

	public int getCoQuanId() {
		return coQuanId;
	}

	public void setCoQuanId(int coQuanId) {
		this.coQuanId = coQuanId;
	}

	public String getTenCoQuan() {
		return tenCoQuan;
	}

	public void setTenCoQuan(String tenCoQuan) {
		this.tenCoQuan = tenCoQuan;
	}

	public List<VanBan> getListVanBan() {
		return listVanBan;
	}

	public void setListVanBan(List<VanBan> listVanBan) {
		this.listVanBan = listVanBan;
	}

	public CoQuan(int coQuanId, String tenCoQuan, List<VanBan> listVanBan) {
		super();
		this.coQuanId = coQuanId;
		this.tenCoQuan = tenCoQuan;
		this.listVanBan = listVanBan;
	}

	public CoQuan() {
		super();
		// TODO Auto-generated constructor stub
	}

}
