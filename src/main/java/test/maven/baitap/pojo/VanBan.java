package test.maven.baitap.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="vanban", catalog="baitap")
public class VanBan implements Cloneable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int vbId;
	
	@Column(unique=true,nullable=false)
	private String soKyHieu;
	
	@Column(nullable=false)
	private String trichYeu;
	
	@Temporal(TemporalType.DATE)
	@Column(length=10)
	private Date ngayBanHanh;
	
	@Temporal(TemporalType.DATE)
	@Column(length=10)
	private Date ngayHieuLuc;
	
	private String nguoiKy;
	
	private String image;
	
	private String fileDinhKem;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coQuanId", nullable=false)
	private CoQuan coQuan;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="loaiVbId", nullable=false)
	private LoaiVB loaiVb;

	public int getVbId() {
		return vbId;
	}

	public void setVbId(int vbId) {
		this.vbId = vbId;
	}

	public String getSoKyHieu() {
		return soKyHieu;
	}

	public void setSoKyHieu(String soKyHieu) {
		this.soKyHieu = soKyHieu;
	}

	public String getTrichYeu() {
		return trichYeu;
	}

	public void setTrichYeu(String trichYeu) {
		this.trichYeu = trichYeu;
	}

	public Date getNgayBanHanh() {
		return ngayBanHanh;
	}
	
	public String getNgayBH() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(ngayBanHanh);
	}

	public void setNgayBanHanh(Date ngayBanHanh) {
		this.ngayBanHanh = ngayBanHanh;
	}

	public Date getNgayHieuLuc() {
		return ngayHieuLuc;
	}

	public void setNgayHieuLuc(Date ngayHieuLuc) {
		this.ngayHieuLuc = ngayHieuLuc;
	}

	public String getNguoiKy() {
		return nguoiKy;
	}

	public void setNguoiKy(String nguoiKy) {
		this.nguoiKy = nguoiKy;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	

	public String getFileDinhKem() {
		return fileDinhKem;
	}

	public void setFileDinhKem(String fileDinhKem) {
		this.fileDinhKem = fileDinhKem;
	}

	public CoQuan getCoQuan() {
		return coQuan;
	}

	public void setCoQuan(CoQuan coQuan) {
		this.coQuan = coQuan;
	}

	public LoaiVB getLoaiVb() {
		return loaiVb;
	}

	public void setLoaiVb(LoaiVB loaiVb) {
		this.loaiVb = loaiVb;
	}

	public VanBan(int vbId, String soKyHieu, String trichYeu, Date ngayBanHanh, Date ngayHieuLuc, String nguoiKy, String image, String fileDinhKem,
			CoQuan coQuan, LoaiVB loaiVb) {
		super();
		this.vbId = vbId;
		this.soKyHieu = soKyHieu;
		this.trichYeu = trichYeu;
		this.ngayBanHanh = ngayBanHanh;
		this.ngayHieuLuc = ngayHieuLuc;
		this.nguoiKy = nguoiKy;
		this.image = image;
		this.fileDinhKem = fileDinhKem;
		this.coQuan = coQuan;
		this.loaiVb = loaiVb;
	}

	public VanBan() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Object clone() throws CloneNotSupportedException{
		   return super.clone();
	   }
	
}
