package test.maven.baitap.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="loaivanban", catalog="baitap")
public class LoaiVB {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY )
	@Column(name="LOAIVB_ID")
	private int loaiVbId;
	
	@Column(name="TENLOAIVANBAN", unique=true, length=255)
	private String tenLoaiVanBan;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "loaiVb", cascade = CascadeType.ALL)
	private List<VanBan> listVanBan;

	
	public int getLoaiVbId() {
		return loaiVbId;
	}


	public void setLoaiVbId(int loaiVbId) {
		this.loaiVbId = loaiVbId;
	}


	public String getTenLoaiVanBan() {
		return tenLoaiVanBan;
	}


	public void setTenLoaiVanBan(String tenLoaiVanBan) {
		this.tenLoaiVanBan = tenLoaiVanBan;
	}


	public List<VanBan> getListVanBan() {
		return listVanBan;
	}


	public void setListVanBan(List<VanBan> listVanBan) {
		this.listVanBan = listVanBan;
	}

	public LoaiVB(int loaiVbId, String tenLoaiVanBan, List<VanBan> listVanBan) {
		super();
		this.loaiVbId = loaiVbId;
		this.tenLoaiVanBan = tenLoaiVanBan;
		this.listVanBan = listVanBan;
	}


	public LoaiVB() {
		super();
		// TODO Auto-generated constructor stub
	}

}
