package test.maven.baitap;

import org.hibernate.Session;

import test.maven.baitap.pojo.CoQuan;
import test.maven.baitap.pojo.LoaiVB;
import test.maven.baitap.pojo.VanBan;
import test.maven.baitap.util.HibernateConnector;


/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Session session = HibernateConnector.getSessionFactory().openSession();
    	session.beginTransaction();
    	CoQuan coQuan = new CoQuan();
    	VanBan vanBan = new VanBan();
    	LoaiVB loaiVb = new LoaiVB();
    	session.close();
    }
}
