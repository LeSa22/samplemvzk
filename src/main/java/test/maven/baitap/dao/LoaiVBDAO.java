package test.maven.baitap.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import test.maven.baitap.pojo.LoaiVB;
import test.maven.baitap.util.HibernateConnector;

public class LoaiVBDAO {
	
	Session session = null;
	@SuppressWarnings("unchecked")
	public List<LoaiVB> getListLVB(){
		List<LoaiVB> listLVB = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from LoaiVB");
			listLVB = query.list();
			//System.out.println("list"+listLVB.size());
			if(listLVB == null)
				return null;
			
			else
				return listLVB;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
}
