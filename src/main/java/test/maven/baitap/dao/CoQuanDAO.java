package test.maven.baitap.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import test.maven.baitap.pojo.CoQuan;
import test.maven.baitap.util.HibernateConnector;

public class CoQuanDAO {
	Session session = null;
	@SuppressWarnings("unchecked")
	public List<CoQuan> getListCoQuan(){
		List<CoQuan> listCoQuan = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from CoQuan");
			listCoQuan = query.list();
			if(listCoQuan == null)
				return null;
			
			else
				return listCoQuan;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
}
