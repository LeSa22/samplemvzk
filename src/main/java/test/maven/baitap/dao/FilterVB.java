package test.maven.baitap.dao;

public class FilterVB {
	private String soKyHieu="";
	private String trichYeu="";
	private String nguoiKy="";
	public String getSoKyHieu() {
		return soKyHieu;
	}
	public void setSoKyHieu(String soKyHieu) {
		this.soKyHieu = soKyHieu==null?"":soKyHieu.trim();
	}
	public String getTrichYeu() {
		return trichYeu;
	}
	public void setTrichYeu(String trichYeu) {
		this.trichYeu = trichYeu==null?"":trichYeu.trim();
	}
	public String getNguoiKy() {
		return nguoiKy;
	}
	public void setNguoiKy(String nguoiKy) {
		this.nguoiKy = nguoiKy==null?"":nguoiKy.trim();
	}
	
	
}
