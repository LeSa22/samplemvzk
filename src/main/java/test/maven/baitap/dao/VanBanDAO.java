package test.maven.baitap.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import test.maven.baitap.pojo.VanBan;
import test.maven.baitap.util.HibernateConnector;

public class VanBanDAO {

	Session session = null;
	public static List<VanBan> listVB = new ArrayList<VanBan>();
	@SuppressWarnings("unchecked")
	public List<VanBan> getListVB() {
		listVB = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from VanBan");
			listVB = query.list();
			// System.out.println("list"+listVB.size());
			if (listVB == null)
				return null;

			else
				return listVB;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<VanBan> getListVbByLoaiVb(int id) {
		listVB = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from VanBan where loaiVbId="+id);
			listVB = query.list();
			if (listVB == null)
				return null;

			else
				return listVB;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<VanBan> getListVbByCoQuan(int id) {
		listVB = new ArrayList<>();
		session = HibernateConnector.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			Query query = session.createQuery("from VanBan where coQuanId="+id);
			listVB = query.list();
			if (listVB == null)
				return null;

			else
				return listVB;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List<VanBan> getFilterVanBan(FilterVB fvb) {
		VanBanDAO vbDao = new VanBanDAO();
		listVB = vbDao.getListVB();
        List<VanBan> vbs = new ArrayList<VanBan>();
        String cat = fvb.getTrichYeu().toLowerCase();
        for (Iterator<VanBan> i = listVB.iterator(); i.hasNext();) {
        	VanBan tmp = i.next();
            if (tmp.getTrichYeu().toLowerCase().contains(cat)|| tmp.getSoKyHieu().toLowerCase().contains(cat)|| tmp.getNguoiKy().toLowerCase().contains(cat) ) {
                vbs.add(tmp);
            }
        }
        return vbs;
    }
	public VanBan addVanBan(VanBan vb) {
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.save(vb);
			transaction.commit();
			return vb;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void deleteVanBan(int id) {
		Session session = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			Transaction beginTransaction = session.beginTransaction();
			Query query = session.createQuery("delete from VanBan v where v.vbId= :id");
			query.setParameter("id", id);
			query.executeUpdate();
			beginTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void update(VanBan vb) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateConnector.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			session.saveOrUpdate(vb);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
