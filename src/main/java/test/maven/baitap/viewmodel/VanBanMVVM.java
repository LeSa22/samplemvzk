package test.maven.baitap.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.fileupload.FileUploadBase.FileUploadIOException;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import test.maven.baitap.dao.CoQuanDAO;
import test.maven.baitap.dao.FilterVB;
import test.maven.baitap.dao.LoaiVBDAO;
import test.maven.baitap.dao.VanBanDAO;
import test.maven.baitap.pojo.CoQuan;
import test.maven.baitap.pojo.LoaiVB;
import test.maven.baitap.pojo.VanBan;

public class VanBanMVVM {
	
	VanBanDAO vbDao = new VanBanDAO();
	LoaiVBDAO lvbDao = new LoaiVBDAO();
	CoQuanDAO cqDao = new CoQuanDAO();

	private Integer curSelectedVBIndex;
	private List<VanBan> listVB = vbDao.getListVB();
	private List<LoaiVB> listLVB;
	private List<CoQuan> listCoQuan;
	private FilterVB filterVb = new FilterVB();
	
	public FilterVB getFilterVb() {
		return filterVb;
	}

	public void setFilterVb(FilterVB filterVb) {
		this.filterVb = filterVb;
	}

	public List<LoaiVB> getListLVB() {
		return lvbDao.getListLVB();
	}

	public List<CoQuan> getListCoQuan() {
		return cqDao.getListCoQuan();
	}
	public void setCurSelectedVBIndex(Integer curSelectedVBIndex) {
		this.curSelectedVBIndex = curSelectedVBIndex; 
	}
	public Integer getCurSelectedVBIndex() {
		return curSelectedVBIndex;
	}
	public List<VanBan> getListVB() {
		return listVB;
	}
	@Init
	public void initSetup() {
		getListVB();
	}
	@Command
	public void addVB(){
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sVanBan", null);
		map.put("recordMode", "NEW");
		Executions.createComponents("addVB.zul", null, map);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Command
	public void delete(@BindingParam("vb") final VanBan vb) {
		String str = "Bạn có muốn xóa văn bản số ký hiệu \"" + vb.getSoKyHieu()
				+ "\" hay không.";

		Messagebox.show(str, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (((Integer) event.getData()).intValue() == Messagebox.OK) {
					vbDao.deleteVanBan(vb.getVbId());
					BindUtils.postNotifyChange(null, null, VanBanMVVM.this, "customerList");
				}
			}
		});
	}
	
	@GlobalCommand
	@NotifyChange("listVB")
	public void updateVanBanList(@BindingParam("pVanBan") VanBan vb,@BindingParam("recordMode") String recordMode ){
		if(recordMode.equals("NEW")){
			try{
				vbDao.addVanBan(vb);
				listVB = vbDao.getListVB();
				Clients.showNotification("Thêm mới thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Thêm mới không thành công!", "error", null, "top_center", 5000);
			}
		}
		if(recordMode.equals("EDIT")){
			try{
				vbDao.update(vb);
				listVB = vbDao.getListVB();
				Clients.showNotification("Cập nhập thành công!", "info", null, "top_center", 5000);
			}catch(Exception e){
				e.printStackTrace();
				Clients.showNotification("Cập nhập không thành công!", "error", null, "top_center", 5000);
			}
		}
	}
	
	@Command
	public void edit(@BindingParam("vb") VanBan vb){
		final HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("sVanBan", vb);
		map.put("recordMode", "EDIT");
		setCurSelectedVBIndex(listVB.indexOf(vb));
		Executions.createComponents("addVB.zul", null, map);
	}
	
	@Command
	@NotifyChange("lisVB")
	public void loadListById(@BindingParam("id") Integer id){
		
		if(listVB.isEmpty())  Messagebox.show("Chưa có văn bản nào!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
		else /*Messagebox.show("Có!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);*/
			listVB = vbDao.getListVbByLoaiVb(id);
	}
	
	@Command
	@NotifyChange("lisVB")
	public void loadListByIdCQ(@BindingParam("id") Integer id){
		listVB = vbDao.getListVbByCoQuan(id);
		if(listVB.isEmpty())  Messagebox.show("Chưa có văn bản nào!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
		else Messagebox.show("Có!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
	@Command
    @NotifyChange("listVB")
    public void changeFilter() {
        listVB = VanBanDAO.getFilterVanBan(filterVb);
    }
	
	@Command
	public void download(@BindingParam("vb") VanBan vb) throws  IOException {
		String foldername = "D:\\gj\\FileUpload\\";
		if(vb.getFileDinhKem() != null){
			String url = foldername + vb.getFileDinhKem();
			if(new File(url).exists()){
				Filedownload.save(new URL("file:///" + url).openStream(), null, vb.getFileDinhKem());
				System.out.println("Djjj " +url );
			}
			else{
				Messagebox.show("Không tìm thấy tệp đính kèm!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
			}
		}
		else Messagebox.show("Không có tệp đính kèm!", "Warning", Messagebox.OK, Messagebox.EXCLAMATION);
	}
	
}
