package test.maven.baitap.viewmodel;

import java.io.File;
import java.io.IOException;
import java.time.Year;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import test.maven.baitap.dao.CoQuanDAO;
import test.maven.baitap.dao.LoaiVBDAO;
import test.maven.baitap.pojo.CoQuan;
import test.maven.baitap.pojo.LoaiVB;
import test.maven.baitap.pojo.VanBan;

public class VBCRUD {
	private VanBan selectedVanBan;
	private String recordMode;
	private LoaiVB loaiVB;
	private CoQuan coQuan;
	private String filePath;
	private String newName;
	private boolean fileuploaded = false;
	private List<LoaiVB> listLVB;
	private List<CoQuan> listCoQuan;
	@Wire("#vbcrud")
	private Window win;
	
	LoaiVBDAO lvbDao = new LoaiVBDAO();
	CoQuanDAO cqDao = new CoQuanDAO();
	
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public boolean isFileuploaded() {
		return fileuploaded;
	}

	public void setFileuploaded(boolean fileuploaded) {
		this.fileuploaded = fileuploaded;
	}
	
	public LoaiVB getLoaiVB() {
		return loaiVB;
	}

	public void setLoaiVB(LoaiVB loaiVB) {
		this.loaiVB = loaiVB;
	}

	public CoQuan getCoQuan() {
		return coQuan;
	}

	public void setCoQuan(CoQuan coQuan) {
		this.coQuan = coQuan;
	}

	public List<LoaiVB> getListLVB() {
		return lvbDao.getListLVB();
	}

	public List<CoQuan> getListCoQuan() {
		return cqDao.getListCoQuan();
	}
	
	public VanBan getSelectedVanBan() {
		return selectedVanBan;
	}

	public void setSelectedVanBan(VanBan selectedVanBan) {
		this.selectedVanBan = selectedVanBan;
	}

	public String getRecordMode() {
		return recordMode;
	}

	public void setRecordMode(String recordMode) {
		this.recordMode = recordMode;
	}
	
	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("sVanBan") VanBan c1,
            @ExecutionArgParam("recordMode") String recordMode) throws CloneNotSupportedException{
		Selectors.wireComponents(view, this, false);
		setRecordMode(recordMode);
		
		if (recordMode.equals("NEW")){
			this.selectedVanBan = new VanBan();
		}
		if (recordMode.equals("EDIT")){
			this.selectedVanBan = (VanBan) c1.clone();
		}
	}
	
	@Command
	public void closeThis(){
		win.detach();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Command
	public void save(){
		Map args = new HashMap<>();
		if(newName!=null)
			selectedVanBan.setFileDinhKem(newName);
		args.put("pVanBan", this.selectedVanBan);
		System.out.println(selectedVanBan.getTrichYeu()+"33333333");
		args.put("recordMode", this.recordMode);
		BindUtils.postGlobalCommand(null, null, "updateVanBanList", args);
		win.detach();
	}
	
	@Command
	public void onUploadPDF(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx) throws IOException {

		UploadEvent upEvent = null;
		Object objUploadEvent = ctx.getTriggerEvent();
		if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
			upEvent = (UploadEvent) objUploadEvent;
		}
		if (upEvent != null) {
			Media media = upEvent.getMedia();
			Calendar now = Calendar.getInstance();
			int year = now.get(Calendar.YEAR);
			int month = now.get(Calendar.MONTH)+1; // Note: zero based!
			int day = now.get(Calendar.DAY_OF_MONTH);
			filePath = "D:/gj/FileUpload/";
			String yearPath =  year + "-" + month + "-" + day + "-";
			newName = yearPath + System.nanoTime()+ media.getName();
			File baseDir = new File(filePath);
			if (!baseDir.exists()) {
				baseDir.mkdirs();
			}
			Files.copy(new File(filePath + newName), media.getStreamData());
			fileuploaded = true;
		}
	}
	
	/*public Validator getFormValidator(){
		System.out.println("DD");
		String soKyHieu = (String)ctx.getProperties("firstName")[0].getValue();
		return new AbstractValidator() {
			
			@Override
			public void validate(ValidationContext ctx) {
				String soKyHieu = (String)ctx.getProperties("firstName")[0].getValue();
				if(soKyHieu == null || soKyHieu.isEmpty())
				{
					addInvalidMessage(ctx, "SoHieuContentError", "Số ký hiệu văn bản bắt buộc nhập");
				}
				else if(soKyHieu.length() < 3)
				{
					addInvalidMessage(ctx, "SoHieuError", "Số ký hiệu văn bản nhỏ hơn 3 ký tự");
				}
				
			}
		};
		return null;
	}*/
}
